![wayland](.assets/wayland.webp)

# IMPORTANT

This repo is currently unmaintained and will forever be, I presume. I have ditched tiling window managers as a whole and have switched to KDE Plasma, so, you know. This repo is dead.

<hr>

# wayland.

dotfiles for wayland.

## Why did I switch to Wayland?

- <details open>
    <summary>Picom.</summary>
    <p>
    At least, the screen tearing can be fixed, but God, the flickering after I turn the PC on from a suspend. It was so problematic I had to add a Polybar switch to turn Picom on/off. Still haven't found a fix.
    </p>
  </details>
- Wanted to try out something new.
- <details open>
    <summary>Configuration.</summary>
    <ul>
    <li>Most configuration is done in the Sway config itself: input configuration, output configuration, Key remapping and stuff. And I don't have to drop a gazillion configuration files in random locations like /etc/X11/xorg.conf.d and a few others just to get stuff working.</li>
    <li>And there's less moving parts: there's no Xorg server, X11, Xorg client, and then the compositor.</li>
  </details>
- Pipewire makes more sense when used within Wayland, since Wayland *needs* Pipewire for video stuff and all.
- Waybar is dope. (`border-radius` for modules! If in Polybar, one would have to create 5 bars for 5 modules, and then launch 'em all with a launch.sh, none of that crap with Waybar)
- `foot` is a great terminal emulator. Fast, minimal and `libsixel` support. What more do I need? (`foot` and `waybar` are Wayland-only btw.)
- <details open>
    <summary>Increased battery life.</summary>
    <p>
    No, I'm not joking, the battery backup has gotten significantly better ever since I ditched Xorg and went full Sway.
    And it's not exactly a reason, but what I experienced after switching to Sway.
    </p>
  </details>
- PINCH-AND-ZOOM WORKS! (accidentally discovered it btw, i have gotten so used to not using pinch-and-zoom because of it not working in xorg)

## Tools

- Compositing Window Manager: [sway](https://github.com/swaywm/sway)
- Terminal Emulator: [foot](https://codeberg.org/dnkl/foot)
- Bar: [waybar](https://github.com/Alexays/Waybar)
- Multiplexer: [tmux-sixel](https://github.com/ChrisSteinbach/tmux-sixel)

- Audio: `pipewire{,-alsa,-jack,-pulse,-media-session}`
- Volume Control: [pulsemixer](https://github.com/GeorgeFilipkin/pulsemixer), pavucontrol (just in case)
- Screen Recorder: [OBS Studio](https://obsproject.com/), [wf-recorder](https://github.com/ammen99/wf-recorder)
- Gamma: [wl-gammactl](https://github.com/mischw/wl-gammactl) (to control contrast, brightness and gamma)
- Screenshot: [grim](https://github.com/emersion/grim), [slurp (for selecting a region)](https://github.com/emersion/slurp)
- Clipboard: [wl-clipboard](https://github.com/bugaevc/wl-clipboard)

- Wallpaper utility: [swaybg](https://github.com/swaywm/swaybg)
- Brightness: [acpilight](https://gitlab.com/wavexx/acpilight) (works everywhere)
- Notifications: [dunst](https://github.com/dunst-project/dunst)
- Run launcher: [rofi-lbonn-wayland](https://github.com/lbonn/rofi) (cause [wofi](https://hg.sr.ht/~scoopta/wofi) doesn't support fullscreen, last time i checked)
- Login Manager: [ly](https://github.com/fairyglade/ly)
- XWayland: `xorg-xwayland` (hope we won't need it in the future)
- Image Viewer: i still use [nsxiv](https://github.com/nsxiv/nsxiv) ([imv](https://sr.ht/~exec64/imv/) if you wanna use pure wayland stuff)
- PDF Reader: [Evince](https://wiki.gnome.org/Apps/Evince)

## Configuration

```
# for enabling Wayland support for QT applications.
echo "export QT_QPA_PLATFORM=wayland" >> ~/.bash_profile

# for enabling Wayland support for Firefox.
echo "export MOZ_ENABLE_WAYLAND=1" >> ~/.bash_profile

# for enabling native Wayland support for Chromium:
echo "--ozone-platform-hint=auto" > ~/.config/chromium-flags.conf
```

## Notes

- `foot` optionally requires [libsixel](https://github.com/libsixel/libsixel) for sixel image support.
- `obs-studio` requires `xdg-desktop-portal-wlr`, qt5-wayland and pipewire.
- `dunst` requires `libnotify`. (obviously)
- If installing `pipewire` and friends on a non-systemd distro, be sure to load 'em up by including 'em in ~/.xinitrc. As for systemd users, no worries.
